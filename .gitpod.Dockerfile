# Select image from https://hub.docker.com/_/php/
FROM php:8.1

RUN apt update -yqq
RUN apt install git libpq-dev libzip-dev zip -yqq

# Install database driver and zip
RUN docker-php-ext-install pdo_mysql pdo_pgsql zip

RUN pecl install mongodb
  
RUN docker-php-ext-enable mongodb

RUN php -m

RUN php -i | grep mongo
  
# Install composer
RUN curl -sS https://getcomposer.org/installer | php

# Install all project dependencies
#RUN php composer.phar install -vvv

RUN useradd -l -u 33333 -G sudo -md /home/gitpod -s /bin/bash -p gitpod gitpod

USER gitpod